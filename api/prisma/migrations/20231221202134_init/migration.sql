-- CreateTable
CREATE TABLE "Health" (
    "api" TEXT NOT NULL,
    "db" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "salt" TEXT
);

-- CreateTable
CREATE TABLE "Role" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" TEXT NOT NULL,
    "userId" INTEGER,
    CONSTRAINT "Role_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Device" (
    "uid" TEXT NOT NULL PRIMARY KEY,
    "type" TEXT NOT NULL,
    "sw_ver" TEXT NOT NULL,
    "hw_ver" TEXT NOT NULL,
    "model" TEXT NOT NULL,
    "alias" TEXT NOT NULL,
    "ip" TEXT NOT NULL,
    "mac" TEXT NOT NULL,
    "led_off" INTEGER NOT NULL,
    "relay_state" INTEGER,
    "child_count" INTEGER,
    "last_seen" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" DATETIME NOT NULL
);

-- CreateTable
CREATE TABLE "Child" (
    "uid" TEXT NOT NULL PRIMARY KEY,
    "id" TEXT NOT NULL,
    "state" INTEGER NOT NULL,
    "alias" TEXT NOT NULL,
    "on_time" INTEGER,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" DATETIME NOT NULL,
    "parent_id" TEXT NOT NULL,
    CONSTRAINT "Child_parent_id_fkey" FOREIGN KEY ("parent_id") REFERENCES "Device" ("uid") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Schedule" (
    "uid" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "cron" TEXT NOT NULL,
    "active" BOOLEAN NOT NULL,
    "state" INTEGER NOT NULL,
    "last_date" DATETIME,
    "next_date" DATETIME,
    "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" DATETIME NOT NULL,
    "device_id" TEXT NOT NULL,
    CONSTRAINT "Schedule_device_id_fkey" FOREIGN KEY ("device_id") REFERENCES "Device" ("uid") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateIndex
CREATE UNIQUE INDEX "Health_api_key" ON "Health"("api");

-- CreateIndex
CREATE UNIQUE INDEX "Health_db_key" ON "Health"("db");

-- CreateIndex
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");

-- CreateIndex
CREATE UNIQUE INDEX "Device_alias_key" ON "Device"("alias");

-- CreateIndex
CREATE UNIQUE INDEX "Device_ip_key" ON "Device"("ip");

-- CreateIndex
CREATE UNIQUE INDEX "Device_mac_key" ON "Device"("mac");

-- CreateIndex
CREATE INDEX "Device_alias_idx" ON "Device"("alias");

-- CreateIndex
CREATE UNIQUE INDEX "Device_alias_ip_mac_key" ON "Device"("alias", "ip", "mac");

-- CreateIndex
CREATE UNIQUE INDEX "Child_id_key" ON "Child"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Child_parent_id_alias_key" ON "Child"("parent_id", "alias");

-- CreateIndex
CREATE UNIQUE INDEX "Schedule_name_key" ON "Schedule"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Schedule_device_id_name_key" ON "Schedule"("device_id", "name");

-- CreateIndex
CREATE UNIQUE INDEX "Schedule_device_id_cron_key" ON "Schedule"("device_id", "cron");
