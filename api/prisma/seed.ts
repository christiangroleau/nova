import { PrismaClient } from '@prisma/client';
import * as bcrypt from 'bcryptjs';
import { Role } from '../src/users/models/role.enum';
import configuration from '../src/shared/configuration';

const prisma = new PrismaClient();

async function main() {
  await initMetaData();
  await initAdmin();
  await initUser('christian', 'password');
}

async function initMetaData() {
  // TODO: change this to upsert
  await prisma.health.create({
    data: { api: configuration().apiVersion, db: configuration().dbVersion },
  });
}

async function initAdmin() {
  const salt = await bcrypt.genSalt();

  const admin = await prisma.user.upsert({
    where: { username: configuration().admin },

    update: {},

    create: {
      username: configuration().admin,
      salt,
      password: await bcrypt.hash(configuration().adminPassword, salt),
      roles: {
        create: [
          {
            name: Role.ADMIN,
          },
        ],
      },
    },
  });

  console.log({ admin });
}

async function initUser(username: string, password: string) {
  const salt = await bcrypt.genSalt();

  const user = await prisma.user.upsert({
    where: { username },

    update: {},

    create: {
      username,
      salt,
      password: await bcrypt.hash(password, salt),
      roles: {
        create: [
          {
            name: Role.USER,
          },
        ],
      },
    },
  });

  console.log({ user });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })

  .catch(async (e) => {
    console.error(e);

    await prisma.$disconnect();

    process.exit(1);
  });
