import { NestFactory } from '@nestjs/core';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { AppModule } from './app.module';
import configuration from './shared/configuration';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({
    transform: true,
  }));

  app.enableVersioning({
    defaultVersion: configuration().apiVersion,
    type: VersioningType.URI,
  });
  app.enableCors(); // TODO: restrict origins
  
  await app.listen(configuration().port);
}
bootstrap();
