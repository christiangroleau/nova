import { Module } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { HealthService } from './health.service';
import { HealthController } from './health.controller';

@Module({
  providers: [HealthService, PrismaService],
  controllers: [HealthController],
})
export class HealthModule {}
