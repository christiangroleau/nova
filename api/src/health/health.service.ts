import { Injectable } from '@nestjs/common';
import { Health } from '@prisma/client';
import { PrismaService } from '../prisma.service';

@Injectable()
export class HealthService {
  constructor(private prisma: PrismaService) {}

  async status(): Promise<Health> {
    return this.prisma.health.findFirstOrThrow();
  }
}
