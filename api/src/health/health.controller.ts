import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { Public } from '../auth/decorators/public.decorator';
import { HealthService } from './health.service';
import { Health } from '@prisma/client';

@Controller('health')
export class HealthController {
  constructor(private healthService: HealthService) {}

  @Get()
  @Public()
  @HttpCode(HttpStatus.OK)
  health(): Promise<Health> {
    return this.healthService.status();
  }
}
