import { Controller, Get, HttpCode, HttpStatus, NotFoundException, Query, Put, Param, Body, UseFilters, ParseIntPipe } from '@nestjs/common';
import { DevicesService } from './devices.service';
import { DatabaseExceptionFilter } from '../shared/db-exception.filter';

@Controller('devices')
@UseFilters(DatabaseExceptionFilter)
export class DevicesController {

    constructor(private service: DevicesService) { }

    @Get()
    @HttpCode(HttpStatus.OK)
    async getDevices() {
        const devices = await this.service.getDevices();

        // TODO: clean this up
        if (!devices.length) {
            throw new NotFoundException('No devices found');
        }

        return { devices };
    }

    @Get('/discover')
    @HttpCode(HttpStatus.OK)
    async discoverDevices(@Query('timeout', ParseIntPipe) timeout: number) {
        // TODO: validate inputs on all requests
        const devices = await this.service.discoverNewDevices(timeout);

        // TODO: clean this up
        if (!devices.length) {
            throw new NotFoundException('No new devices found');
        }

        return { devices };
    }

    @Get('/alias')
    @HttpCode(HttpStatus.OK)
    async getDeviceByAlias(@Query('value') value: string) {
        // TODO: validate inputs on all requests
        const device = await this.service.getDeviceByAlias(value!);

        if (!device) {
            throw new NotFoundException(`No device with name '${value}' found`);
        }

        return { device };
    }

    @Put('/alias/:alias')
    @HttpCode(HttpStatus.CREATED)
    async updateDeviceAlias(@Param('alias') alias: string, @Body('alias') newAlias: string) {
        // TODO: validate inputs on all requests
        const device = await this.service.updateDeviceAlias(alias!, newAlias!);

        if (!device) {
            throw new NotFoundException(`No device with name '${alias}' found`);
        }

        return { device };
    }

    @Put('/alias/:alias/children/:childAlias')
    @HttpCode(HttpStatus.CREATED)
    async updateDeviceChildAlias(@Param('alias') alias: string, @Param('childAlias') childAlias: string, @Body('alias') newAlias: string) {
        // TODO: validate inputs on all requests
        const child = await this.service.updateChildDeviceAlias(alias, childAlias, newAlias);

        if (!child) {
            throw new NotFoundException(`No child device with name '${childAlias}' found`);
        }

        return { child };
    }

    @Put('/alias/:alias/relay')
    @HttpCode(HttpStatus.CREATED)
    async updateDeviceRelay(@Param('alias') alias: string, @Body('relay_state', ParseIntPipe) state: number) {
        // TODO: validate inputs on all requests
        const device = await this.service.updateDeviceRelay(alias!, Number(state));

        if (!device) {
            throw new NotFoundException(`No device with name '${alias}' found`);
        }

        return { device };
    }

    @Put('/alias/:alias/relay/children/:childAlias')
    @HttpCode(HttpStatus.CREATED)
    async updateChildDeviceRelay(@Param('alias') alias: string, @Param('childAlias') childAlias, @Body('relay_state', ParseIntPipe) state: number) {
        // TODO: validate inputs on all requests
        const child = await this.service.updateChildDeviceRelay(alias, childAlias, Number(state));

        if (!child) {
            throw new NotFoundException(`No child device with name '${childAlias}' found`);
        }

        return { child };
    }
}
