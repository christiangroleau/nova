// import { Schedule } from "../../schedules/models/schedule.model";
import { Schedule } from "../../schedules/models/schedule.model";
import { ChildDevice } from "./device-child.model";

export class Device2 {
    type: string;
    sw_ver: string;
    hw_ver: string;
    model: string;
    alias: string;
    ip: string;
    mac: string;
    led_off: number;
    relay_state: number;
    child_count: number;
    children: ChildDevice[]
    // schedule?: Schedule
    // scheduleId: string;
    schedule?: Schedule[]
}
