export class DeviceDetails {
    type: string;
    sw_ver: string;
    hw_ver: string;
    model: string;
    alias: string;
    ip: string;
    mac: string;
    led_off: number;
    relay_state: number;
    // on_time: number;
    // dev_name: string;
    child_count: number;
}