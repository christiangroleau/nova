import { ChildDevice } from "./device-child.model";
import { DeviceDetails } from "./device-details.model";

export class Device {
    ip: string;
    details: DeviceDetails;
    children: ChildDevice[]
}
