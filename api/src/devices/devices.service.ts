import { Injectable, NotFoundException, OnApplicationBootstrap } from '@nestjs/common';
import { Child as PrismaChild, Device as PrismaDevice } from '@prisma/client';
import { PrismaService } from '../prisma.service';
import { TpLinkService } from '../tplink/tplink.service';
import { Device2 } from './models/device2.model';



export type DeviceAndChildren = PrismaDevice & {
  children: PrismaChild[];
};

@Injectable()
export class DevicesService implements OnApplicationBootstrap {
  constructor(private tplink: TpLinkService, private prisma: PrismaService) { }

  async onApplicationBootstrap() {
    // TODO: logger
    console.log('Discovering all devices...');
    this.discoverNewDevices();
  }

  // TODO: add a 'simple' option and only return most important data
  // deep queries every live device
  async getDevices() {
    return this.prisma.device.findMany({ include: { children: false, schedules: true } });

    // return await this.tplink.getDevices(devices.map(d => d.ip));
  }

  // TODO: clean up the response type
  async discoverNewDevices(timeout: number = 2000) {
    const devices: Device2[] = await this.tplink.discover(timeout);

    // TODO: keep list of upserted rows and return that instead?
    // list of row to be upserted
    for (const device of devices) {
      await this.prisma.device.upsert({
        where: { alias: device.alias },
        update: {},
        create: {
          ...device,
          children: {
            create: device.children,
          },
          schedules: {
            create: [] // TODO: what happens to the schedule if a device disappears for a while and comes back online?
          }
        },
        include: { children: true },
      })
    }

    return devices.length ? devices : await this.getDevices();;
  }

  // TODO: returns prisma device
  async getDeviceByAlias(alias: string) {
    return await this.prisma.device.findUniqueOrThrow({
      where: {
        alias,
      },
      include: {
        children: true,
        schedules: true
      },
    });
  }

  async getDeviceChildByAlias(device: any, childAlias: string) {
    // find the specified child device
    const child = device.children.filter((c) => c.alias === childAlias)[0];

    if (!child) {
      throw new NotFoundException(`Specified child device '${childAlias}' can not be found`);
    }

    return child;
  }

  // TODO: returns prisma device
  async updateDeviceAlias(alias: string, newAlias: string) {
    let device = await this.getDeviceByAlias(alias);

    const success = this.tplink.updateAlias(device.ip, newAlias);

    if (success) {
      device = await this.prisma.device.update({
        where: { alias },
        data: { alias: newAlias },
        include: {
          children: true,
          schedules: true
        },
      });
    }

    return device;
  }

  // TODO: streamline this to inside of updateDeviceAlias
  // TODO: returns prisma child device
  async updateChildDeviceAlias(alias: string, childAlias: string, newAlias: string) {
    let device = await this.getDeviceByAlias(alias);
    let child = await this.getDeviceChildByAlias(device, childAlias);

    const success = await this.tplink.updateChildAlias(device.ip, child.id, newAlias);

    if (success) {
      child = await this.prisma.child.update({
        where: { id: child.id },
        data: { alias: newAlias },
        include: { parent: true },
      });
    }

    return child;
  }

  async updateDeviceRelay(alias: string, state: number) {
    let device = await this.getDeviceByAlias(alias);

    const success = this.tplink.updateRelay(device.ip, state ? true : false);

    if (success) {
      device = await this.prisma.device.update({
        where: { alias },
        data: { relay_state: state },
        include: {
          children: true,
          schedules: true
        },
      });
    }

    return device;
  }

  // TODO: streamline this to inside of updateDeviceRelay
  // TODO: returns prisma child device
  async updateChildDeviceRelay(alias: string, childAlias: string, state: number) {
    let device = await this.getDeviceByAlias(alias);
    let child = await this.getDeviceChildByAlias(device, childAlias);

    const success = this.tplink.updateChildRelay(device.ip, child.id, state ? true : false);

    if (success) {
      child = await this.prisma.child.update({
        where: {
          parent_id_alias: {
            parent_id: device.uid,
            alias: childAlias,
          },
        },
        data: {
          state,
        },
      });
    }

    return child;
  }
}
