import { Module } from '@nestjs/common';
import { DevicesService } from './devices.service';
import { DevicesController } from './devices.controller';
import { PrismaService } from '../prisma.service';
import { TpLinkService } from '../tplink/tplink.service';

@Module({
  providers: [DevicesService, TpLinkService, PrismaService],
  controllers: [DevicesController]
})
export class DevicesModule { }
