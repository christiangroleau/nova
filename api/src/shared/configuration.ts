export default () => ({
  // meta
  apiVersion: `${process.env.API_VERSION}`,
  dbVersion: `${process.env.DB_VERSION}`,

  // seed admin account
  admin: `${process.env.ADMIN_USERNAME}`,
  adminPassword: `${process.env.ADMIN_PASSWORD}`,

  // auth
  jwtExpiration: `${process.env.JWT_EXPIRATION}`,
  jwt: `${process.env.JWT_SECRET_KEY}`,

  // server
  port: parseInt(process.env.PORT, 10),
  database: {
    host: `${process.env.DATABASE_HOST}`,
    port: parseInt(process.env.DATABASE_PORT, 10),
  },
});
