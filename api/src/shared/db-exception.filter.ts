
import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { Prisma } from '@prisma/client';

@Catch(Prisma.PrismaClientKnownRequestError)
export class DatabaseExceptionFilter implements ExceptionFilter {
    catch(exception: Prisma.PrismaClientKnownRequestError, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        if (exception.code === 'P2002') {
            const message = 'Resource already exists';
            response
                .status(409)
                .json(message);
        }

        if (exception.code === 'P2025') {
            const message = 'Specified resource does not exist';
            response
                .status(404)
                .json(message);
        }
    }
}
