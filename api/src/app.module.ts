import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';

import { AuthModule } from './auth/auth.module';
import { HealthModule } from './health/health.module';
import { UsersModule } from './users/users.module';
import { DevicesModule } from './devices/devices.module';
import { TpLinkModule } from './tplink/tplink.module';
import { SchedulesModule } from './schedules/schedules.module';
import configuration from './shared/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
    ScheduleModule.forRoot(),
    AuthModule,
    UsersModule,
    HealthModule,
    DevicesModule,
    TpLinkModule,
    SchedulesModule,
  ],
})
export class AppModule { }
