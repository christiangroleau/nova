import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { JwtToken } from './models/token';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) { }

  async register(username: string, password: string): Promise<JwtToken> {
    const user = await this.usersService.createUser({ username, password });

    const payload = {
      user,
      sub: user.id,
      name: user.username,
    };

    return {
      access_token: this.jwtService.sign(payload),
    } as JwtToken;
  }

  async login(username: string, password: string): Promise<JwtToken> {
    const user = await this.usersService.validate(username, password);

    if (!user) {
      throw new UnauthorizedException();
    }

    const payload = {
      user,
      sub: user.id,
      name: user.username,
    };

    return {
      access_token: this.jwtService.sign(payload),
    } as JwtToken;
  }
}
