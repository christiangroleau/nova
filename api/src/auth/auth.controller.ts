import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Request,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { Public } from './decorators/public.decorator';
import { CredentialsDto } from './dto/credentials.dto';
import { JwtToken } from './models/token';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) { }

  @Post('register')
  @Public()
  @HttpCode(HttpStatus.CREATED)
  async register(@Body() credentials: CredentialsDto): Promise<JwtToken> {
    return this.authService.register(credentials.username, credentials.password);
  }

  @Post('login')
  @Public()
  @HttpCode(HttpStatus.OK)
  async login(@Body() credentials: CredentialsDto): Promise<JwtToken> {
    return this.authService.login(credentials.username, credentials.password);
  }

  @Get('profile')
  @HttpCode(HttpStatus.OK)
  async profile(@Request() req) {
    return req.user;
  }
}
