import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Request,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { Role } from './models/role.enum';
import { Roles } from './decorators/roles.decorator';
import { UsersService } from './users.service';
import { User } from './models/user.model';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) { }

  @Post()
  @Roles(Role.ADMIN)
  @HttpCode(HttpStatus.CREATED)
  create(@Body() userDto: CreateUserDto): Promise<Partial<User>> {
    return this.usersService.createUser({
      username: userDto.username,
      password: userDto.password,
    });
  }

  @Get()
  @Roles(Role.ADMIN)
  findAll(): Promise<Partial<User>[]> {
    return this.usersService.users({});
  }

  @Get('me')
  @HttpCode(HttpStatus.OK)
  getProfile(@Request() req): Partial<User> {
    return req.user;
  }

  @Get(':name')
  @Roles(Role.ADMIN)
  findByName(@Param() params: any): Promise<Partial<User>> {
    return this.usersService.user({ username: params.name });
  }
}
