import { ConflictException, Injectable } from '@nestjs/common';
import { User, Prisma } from '@prisma/client';
import * as bcrypt from 'bcryptjs';
import { PrismaService } from '../prisma.service';
import { Role } from './models/role.enum';

export type UserResponse = Omit<User, "password" | "salt">;

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) { }

  async createUser(userCreateInput: Prisma.UserCreateInput): Promise<User> {
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash(userCreateInput.password, salt);
    try {
      return await this.prisma.user.create({
        data: {
          username: userCreateInput.username,
          password,
          salt,
          roles: {
            create: [
              {
                name: Role.USER, // default role is user
              },
            ],
          },
        },
      });
    } catch (error) {
      if (error instanceof Prisma.PrismaClientKnownRequestError) {
        if (error.code === 'P2002') {
          // TODO: proper logging
          throw new ConflictException('User with this name already exists');
        }
      }
      // throw e;
    }
  }

  // TODO: this function is used internally and should a full User object. Make private?
  async user(userWhereUniqueInput: Prisma.UserWhereUniqueInput): Promise<User | null> {
    const user = await this.prisma.user.findUnique({
      where: userWhereUniqueInput,
      include: {
        roles: true,
      },
    });

    return user;
  }

  async users(params: {
    skip?: number,
    take?: number,
    cursor?: Prisma.UserWhereUniqueInput,
    where?: Prisma.UserWhereInput,
    orderBy?: Prisma.UserOrderByWithRelationInput
  }): Promise<UserResponse[]> {
    const { skip, take, cursor, where, orderBy } = params;
    const users = await this.prisma.user.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });

    return users.map((user) => this.sanitize(user));
  }

  async updateUser(params: { where: Prisma.UserWhereUniqueInput, data: Prisma.UserUpdateInput }): Promise<UserResponse> {
    const { where, data } = params;
    const user = await this.prisma.user.update({
      data,
      where,
    });

    return this.sanitize(user);
  }

  async deleteUser(where: Prisma.UserWhereUniqueInput): Promise<UserResponse> {
    const user = await this.prisma.user.delete({
      where,
    });

    return this.sanitize(user);
  }

  async validate(username: string, password: string): Promise<UserResponse> {
    const user = await this.user({ username });

    if (user && user.password === (await bcrypt.hash(password, user.salt))) {
      return this.sanitize(user);
    }

    return null;
  }

  private sanitize(user: User): UserResponse {
    if (user) {
      const { password, salt, ...rest } = user;

      return rest;
    }
    return null;
  }

}
