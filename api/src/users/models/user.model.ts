import { Role } from './role.model';

// This should be a real class/interface representing a user entity
export class User {
  id: number;
  username: string;
  password: string;
  salt: string;
  roles: Role[];

  constructor(
    id: number,
    username: string,
    password: string,
    salt: string,
    roles: Role[],
  ) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.salt = salt;
    this.roles = roles;
  }
}
