import { Injectable, OnModuleInit } from '@nestjs/common';
import { AnyDevice, Client } from 'tplink-smarthome-api';
import { hasSysinfoChildren } from 'tplink-smarthome-api/lib/plug';
import { isPlugSysinfo } from 'tplink-smarthome-api/lib/device';
import { Device2 } from '../devices/models/device2.model';

@Injectable()
export class TpLinkService extends Client implements OnModuleInit {

    client: Client;

    async onModuleInit() {
        this.client = new Client();
    }

    async getDevices(ips: string[]): Promise<Device2[]> {
        const devices: Device2[] = [];
        for (const ip of ips) {
            devices.push(await this.mapSingle(await this.client.getDevice({ host: ip })));
        }

        return devices;
    }

    async discover(timeout: number): Promise<Device2[]> {
        const discovered: AnyDevice[] = await new Promise((resolve, _) => {
            // list of newly discovered devices
            let devices: AnyDevice[] = [];

            // kill discovery at timeout
            setTimeout(() => {
                this.client.stopDiscovery();
                this.client.removeAllListeners();
                resolve(devices);
            }, timeout);

            this.client.startDiscovery()
                .on('device-new', (device: AnyDevice) => {
                    devices.push(device);
                });
        });

        return await this.mapMany(discovered);
    }

    async updateAlias(ip: string, alias: string): Promise<boolean> {
        const device = await this.client.getDevice({ host: ip });

        return await device.setAlias(alias);
    }

    // TODO: streamline this to inside of updateAlias
    async updateChildAlias(ip: string, childId: string, newAlias: string): Promise<boolean> {
        const device = await this.client.getDevice({ host: ip, childId })

        return await device.setAlias(newAlias);
    }

    async updateRelay(ip: string, state: boolean): Promise<boolean> {
        const device = await this.client.getDevice({ host: ip });

        return await device.setPowerState(state);
    }

    // TODO: streamline this to inside of updateRelay
    async updateChildRelay(ip: string, childId: string, state: boolean): Promise<boolean> {
        const device = await this.client.getDevice({ host: ip, childId });

        return await device.setPowerState(state);
    }

    async mapMany(discovered: AnyDevice[]): Promise<Device2[]> {
        // multi-socket plugs only differentiate by alias but have same mac address
        // exclude duplicate mac addresses
        const devices: Map<string, Device2> = new Map();

        for (const found of discovered) {
            const device = await this.mapSingle(found);
            devices.set(device.mac, device);
        }

        return Array.from(devices.values());
    }

    async mapSingle(device: AnyDevice): Promise<Device2> {
        const si = await device.getSysInfo();

        const d: Device2 = {
            type: device.deviceType,
            sw_ver: device.softwareVersion,
            hw_ver: device.hardwareVersion,
            model: device.model,
            alias: si.alias,
            ip: device.host,
            mac: device.mac,
            led_off: isPlugSysinfo(si) ? si.led_off : null,
            relay_state: isPlugSysinfo(si) ? si.relay_state : null,
            children: [],
            child_count: 0,
        };

        if (hasSysinfoChildren(si)) {
            d.children = si.children.map((c) => { return { id: c.id, alias: c.alias, state: c.state } });
            d.relay_state = d.children.some((c) => c.state === 1) ? 1 : 0
            d.child_count = d.children.length
        }

        return d;
    }
}
