import { Test, TestingModule } from '@nestjs/testing';
import { TpLinkService } from './tplink.service';

describe('TpLinkService', () => {
  let service: TpLinkService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TpLinkService],
    }).compile();

    service = module.get<TpLinkService>(TpLinkService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
