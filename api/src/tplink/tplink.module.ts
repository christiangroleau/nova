import { Module } from '@nestjs/common';
import { TpLinkService } from './tplink.service';

@Module({
    providers: [TpLinkService],
    exports: [TpLinkService]
})
export class TpLinkModule { }
