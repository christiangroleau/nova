import { Module } from '@nestjs/common';
import { SchedulerRegistry } from '@nestjs/schedule';

import { SchedulesController } from './schedules.controller';
import { SchedulesService } from './schedules.service';
import { PrismaService } from '../prisma.service';
import { DevicesService } from '../devices/devices.service';
import { TpLinkService } from '../tplink/tplink.service';

@Module({
  providers: [SchedulesService, TpLinkService, DevicesService, SchedulerRegistry, PrismaService],
  controllers: [SchedulesController],
})
export class SchedulesModule { }
