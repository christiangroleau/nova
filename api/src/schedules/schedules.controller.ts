import { Controller, Get, Post, HttpCode, HttpStatus, NotFoundException, Param, Body, Delete } from '@nestjs/common';
import { SchedulesService } from './schedules.service';
import { CreateScheduleDto } from './dtos/create-schedule.dto';
import { DeleteScheduleDto } from './dtos/delete-schedule.dto';

// TODO: timezone in .env file

@Controller('schedules')
export class SchedulesController {
    constructor(private service: SchedulesService) { }

    @Get()
    @HttpCode(HttpStatus.OK)
    async getSchedules() {
        const schedules = await this.service.getSchedules();

        if (!schedules.length) {
            throw new NotFoundException('No schedules found');
        }

        return { schedules };
    }

    @Get('/name/:name')
    @HttpCode(HttpStatus.OK)
    async getScheduleByName(@Param('name') name: string) {
        const schedule = await this.service.getScheduleByName(name);

        return { schedule };
    }

    @Get('/device/:alias')
    @HttpCode(HttpStatus.OK)
    async getScheduleForDevice(@Param('alias') alias: string) {
        const schedules = await this.service.getSchedulesForDevice(alias);

        return { schedules };
    }

    @Post('/name/:name')
    @HttpCode(HttpStatus.CREATED)
    async createSchedule(@Param('name') name: string, @Body() createScheduleDto: CreateScheduleDto) {
        const schedule = await this.service.createSchedule(name, createScheduleDto);

        return { schedule };
    }

    @Delete('/name/:name')
    async deleteSchedule(@Param('name') name: string, @Body() deleteScheduleDto: DeleteScheduleDto) {
        const schedule = await this.service.deleteSchedule(name, deleteScheduleDto);

        return { schedule };
    }
}
