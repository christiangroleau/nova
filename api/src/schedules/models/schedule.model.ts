import { Device2 } from "../../devices/models/device2.model";

export class Schedule {
    name: string;
    cron: string;
    state: number;
    // TODO: map Prisma model to outgoing service model
    lastDate: Date;
    nextDate: Date;

    // device: Device2;
}