export class CreateScheduleDto {
    alias: string;
    cron: string;
    state: number;
}
