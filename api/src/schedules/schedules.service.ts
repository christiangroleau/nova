import { Injectable, NotFoundException, OnApplicationBootstrap } from '@nestjs/common';
import { SchedulerRegistry } from '@nestjs/schedule';
import { CronJob } from 'cron';

import { PrismaService } from '../prisma.service';
import { DevicesService } from '../devices/devices.service';
import { CreateScheduleDto } from './dtos/create-schedule.dto';
import { DeleteScheduleDto } from './dtos/delete-schedule.dto';

@Injectable()
export class SchedulesService implements OnApplicationBootstrap {

    constructor(
        private devicesService: DevicesService,
        private schedulerRegistry: SchedulerRegistry,
        private prisma: PrismaService) { }

    async onApplicationBootstrap() {
        // TODO: logger
        console.log('Registering all existing schedules...');
        const schedules = await this.prisma.schedule.findMany({ include: { device: true } });

        for (const schedule of schedules) {
            await this.setSchedule({
                name: schedule.name,
                cron: schedule.cron,
                alias: schedule.device.alias,
                deviceUid: schedule.device.uid,
                state: schedule.state
            });
        }
    }

    async getSchedules() {
        return this.prisma.schedule.findMany({ include: { device: true } });
    }

    async getScheduleByName(name: string) {
        return this.prisma.schedule.findUniqueOrThrow({ where: { name }, include: { device: true } });
    }

    async getSchedulesForDevice(alias: string) {
        return this.prisma.schedule.findMany({ where: { device: { alias } }, include: { device: true } });
    }

    async createSchedule(name: string, createScheduleDto: CreateScheduleDto) {
        let schedule = {};
        const device = await this.devicesService.getDeviceByAlias(createScheduleDto.alias);
        let exists = null;
        try {
            exists = this.schedulerRegistry.getCronJob(name);
        }
        catch (e) {
            // TODO: logger
        }

        if (!exists) {
            // TODO: do what if the schedule already exists?
            schedule = await this.setSchedule({
                name,
                cron: createScheduleDto.cron,
                alias: createScheduleDto.alias,
                deviceUid: device.uid,
                state: createScheduleDto.state
            });
        } else {
            schedule = await this.prisma.schedule.findFirstOrThrow({ where: { name } });
        }

        return schedule;
    }

    async deleteSchedule(name: string, deleteScheduleDto: DeleteScheduleDto) {
        // find device
        const device = await this.devicesService.getDeviceByAlias(deleteScheduleDto.alias);

        let job = null;
        try {
            job = this.schedulerRegistry.getCronJob(name);
            this.schedulerRegistry.deleteCronJob(name);
        }
        catch (e) {
            // TODO: logger
            throw new NotFoundException(`Specified schedule '${name}' can not be found`);
        }

        // delete and stop schedule
        const schedule = await this.prisma.schedule.delete({
            where: {
                device_id_name: {
                    device_id: device?.uid!,
                    name,
                },
            },
        });

        job?.stop();

        return schedule;
    }

    async pauseSchedule(name: string, alias: string) {
        const device = await this.devicesService.getDeviceByAlias(alias);

        let schedule = await this.prisma.schedule.findUniqueOrThrow({
            where: {
                device_id_name: {
                    device_id: device.uid,
                    name: name,
                }
            }
        });

        this.schedulerRegistry.getCronJob(name).stop();

        schedule = await this.prisma.schedule.update({
            where: {
                device_id_name: {
                    device_id: device.uid,
                    name: name,
                }
            },
            data: {
                active: false
            }
        });

        return schedule;
    }

    private async updateSchedule(name: string, alias: string, data: { name?: string, cron?: string, active?: boolean, state?: number }) {
        throw Error('Not implemented yet');
        const device = await this.devicesService.getDeviceByAlias(alias);

        let schedule = await this.prisma.schedule.findUniqueOrThrow({
            where: {
                device_id_name: {
                    device_id: device.uid,
                    name: name,
                }
            }
        });

        this.schedulerRegistry.getCronJob(name).stop();

        schedule = await this.prisma.schedule.update({
            where: {
                device_id_name: {
                    device_id: device.uid,
                    name: name,
                }
            },
            // TODO: accept all data properties
            data: {
                active: data.active
            }
        });

        return schedule;
    }

    private async setSchedule(input: { name: string, cron: string, alias: string, deviceUid: string, state: number, }) {
        const job = new CronJob(input.cron, () => {
            // TODO: logger
            console.log(`Cron ${input.name} for ${input.alias} state ${input.state} (${input.cron})`);
            this.devicesService.updateDeviceRelay(input.alias, input.state);
        });

        this.schedulerRegistry.addCronJob(input.name, job);

        // TODO: unique constraint failed when attempting to add same named schedule
        const schedule = await this.prisma.schedule.upsert({
            where: {
                device_id_name: {
                    device_id: input.deviceUid,
                    name: input.name,
                },
            },
            update: {
                last_date: job.lastDate(),
                // next_date: job.lastDate(),
                next_date: job.nextDate().toJSDate(),
            },
            create: {
                name: input.name,
                cron: input.cron,
                active: true,
                state: input.state,
                last_date: job.lastDate(),
                // next_date: job.lastDate(),
                next_date: job.nextDate().toJSDate(),
                device_id: input.deviceUid,
            },
        });

        job.start();

        return schedule;
    }
}
