import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AuthModule } from '../src/auth/auth.module';
import { PrismaTestUtilitiesService } from './utils/prisma-test-utils';

describe('When a user looks up their autho profile...', () => {
  let app: INestApplication;
  let prismaUtils: PrismaTestUtilitiesService;
  const user = { username: 'profile-john', password: 'password' };
  let accessToken = '';

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AuthModule],
      providers: [PrismaTestUtilitiesService] // use test prisma service
    }).compile();

    app = moduleRef.createNestApplication();
    prismaUtils = app.get<PrismaTestUtilitiesService>(PrismaTestUtilitiesService);
    await app.init();
  });

  beforeEach(async () => {
    // capture the auth token prior to all of the tests
    await prismaUtils.createUser(user.username, user.password);
    const response = await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: user.username, password: user.password });

    accessToken = response.body.access_token;
  });

  afterEach(async () => {
    await prismaUtils.deleteUser(user.username);
  });

  afterAll(async () => {
    await prismaUtils.$disconnect();
    await app.close();
  });

  it(`/GET /auth/profile -- should contain all of the JWT items`, async () => {
    // act & assert
    const response = await request(app.getHttpServer())
      .get('/auth/profile')
      .set('Authorization', `Bearer ${accessToken}`);

    expect(response.status).toEqual(200);
    expect(response.body.username).toBe(user.username);
    expect(response.body.hasOwnProperty('password')).toBeFalsy();
  });
});
