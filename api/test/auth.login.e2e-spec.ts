import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AuthModule } from '../src/auth/auth.module';
import { PrismaTestUtilitiesService } from './utils/prisma-test-utils';

describe('When a user logs in...', () => {
  let app: INestApplication;
  let prismaUtils: PrismaTestUtilitiesService;
  const user = { username: 'login-john', password: 'password' };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AuthModule],
      providers: [PrismaTestUtilitiesService] // use test prisma service
    }).compile();

    app = moduleRef.createNestApplication();
    prismaUtils = app.get<PrismaTestUtilitiesService>(PrismaTestUtilitiesService);
    await app.init();
  });

  beforeEach(async () => {
    await prismaUtils.createUser(user.username, user.password);
  });

  afterEach(async () => {
    await prismaUtils.deleteUser(user.username);
  })

  afterAll(async () => {
    await prismaUtils.$disconnect();
    await app.close();
  });

  it(`/POST /auth/login -- should not auth a user with an incorrect password`, () => {
    // arrange
    const res = { message: 'Unauthorized', statusCode: 401 };

    // act & assert
    return request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: user.username, password: 'unauthorized' })
      .expect(401)
      .expect(res);
  });

  it(`/POST /auth/login -- should auth a user with a correct password`, () => {
    // act & assert
    return request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: user.username, password: user.password })
      .expect(200)
      .expect(function (res) {
        if (!res.body.hasOwnProperty('access_token'))
          throw new Error("Expected 'access_token' key!");
      });
  });
});
