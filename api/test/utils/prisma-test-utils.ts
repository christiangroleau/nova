import { Injectable } from '@nestjs/common';
import { UsersService } from '../../src/users/users.service';
import { PrismaService } from '../../src/prisma.service';

/*
    This class uses the same prisma services as the application.
    It was created to provide test utilities for e2e tests.
*/

// TODO see: https://github.com/jmcdo29/testing-nestjs/blob/main/apps/prisma-sample/src/prisma/prisma.service.ts
@Injectable()
export class PrismaTestUtilitiesService extends PrismaService {
    userService = new UsersService(this);

    async truncate() {
        await this.$queryRawUnsafe(`PRAGMA foreign_keys = ON;`);
        const tables = await this.$queryRawUnsafe<Array<any>>(`SELECT * FROM sqlite_master WHERE type='table';`);
        tables.forEach((table) => this.truncateTable(table['tablename'])); // TODO: without await here?
    }

    async truncateTable(tablename) {
        if (tablename === undefined || tablename === '_prisma_migrations') {
            return;
        }

        try {
            await this.$executeRawUnsafe(`DELETE FROM ${tablename};`);
        } catch (error) {
            console.log({ error });
        }
    }

    async createUser(username: string, password: string) {
        await this.userService.createUser({ username, password });
    }

    async deleteUser(username: string) {
        const found = await this.userService.user({ username });

        if (found) {
            await this.userService.deleteUser({ id: found.id });
            return true;
        }

        return false;
    }
}