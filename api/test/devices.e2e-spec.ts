import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { DevicesModule } from '../src/devices/devices.module';
import { PrismaTestUtilitiesService } from './utils/prisma-test-utils';

describe('When a user interacts with devices...', () => {
    let app: INestApplication;
    let prismaUtils: PrismaTestUtilitiesService;
    const user = { username: 'profile-john', password: 'password' };
    let accessToken = '';

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [DevicesModule],
            providers: [PrismaTestUtilitiesService] // use test prisma service
        }).compile();

        app = moduleRef.createNestApplication();
        app.useGlobalPipes(new ValidationPipe({
            transform: true,
        }));
        prismaUtils = app.get<PrismaTestUtilitiesService>(PrismaTestUtilitiesService);
        await app.init();
    });

    beforeAll(async () => {
        // capture the auth token prior to all of the tests
        await prismaUtils.createUser(user.username, user.password);
        const response = await request(app.getHttpServer())
            .post('/auth/login')
            .send({ username: user.username, password: user.password });

        accessToken = response.body.access_token;
    });

    afterAll(async () => {
        await prismaUtils.deleteUser(user.username);
        await prismaUtils.$disconnect();
        await app.close();
    });

    describe('and discovers devices...', () => {
        it(`/GET /devices/discover -- should return a list of all discovered devices`, async () => {
            // act
            const response = await request(app.getHttpServer())
                .get('/devices/discover?timeout=1000')
                .set('Authorization', `Bearer ${accessToken}`);

            // assert
            expect(response.status).toEqual(200);
            expect(response.body.length).toBeGreaterThan(0);
        });

        it(`/GET /devices/discover -- should 400 when timeout is invalid`, async () => {
            // act
            const response = await request(app.getHttpServer())
                .get('/devices/discover?timeout=INVALID') // wrong timeout value
                .set('Authorization', `Bearer ${accessToken}`);

            // assert
            expect(response.status).toEqual(400);
        });

        it(`/GET /devices -- should return a list of all pre-discovered devices`, async () => {
            // act
            const response = await request(app.getHttpServer())
                .get('/devices')
                .set('Authorization', `Bearer ${accessToken}`);

            // assert
            expect(response.status).toEqual(200);
            expect(response.body.length).toBeGreaterThan(0);
        });
    });

    describe('and queries specific devices...', () => {
        it('/GET /devices/alias?value=... -- should return a device with specified name', async () => {
            // arrange
            const alias = 'test-plug';

            // act
            const response = await request(app.getHttpServer())
                .get(`/devices/alias?value=${alias}`)
                .set('Authorization', `Bearer ${accessToken}`);

            // assert
            expect(response.status).toEqual(200);
            expect(response.body.alias).toBe(alias);
        });

        it('/GET /devices/alias?value=... -- should 404 when the alias does not exist', async () => {
            // arrange
            const alias = 'does-not-exist';

            // act
            const response = await request(app.getHttpServer())
                .get(`/devices/alias?value=${alias}`)
                .set('Authorization', `Bearer ${accessToken}`);

            // assert
            expect(response.status).toEqual(404);
        });

        it('/PUT /devices/alias/:alias -- should set new specified name', async () => {
            // arrange
            const currentAlias = 'test-plug';
            const payload = { alias: 'test-plug' };

            // act
            const response = await request(app.getHttpServer())
                .put(`/devices/alias/${currentAlias}`)
                .set('Authorization', `Bearer ${accessToken}`)
                .send(payload);

            // assert
            expect(response.status).toEqual(201);
            expect(response.body.alias).toBe(payload.alias);
        });

        it('/PUT /devices/alias/:alias/children/:childAlias -- should set new specified child name', async () => {
            // arrange
            const currentAlias = 'spare-outdoor';
            const currentChildAlias = 'test-child-plug-1';
            const payload = { alias: 'test-child-plug-1' };

            // act
            const response = await request(app.getHttpServer())
                .put(`/devices/alias/${currentAlias}/children/${currentChildAlias}`)
                .set('Authorization', `Bearer ${accessToken}`)
                .send(payload);

            // assert
            expect(response.status).toEqual(201);
            expect(response.body.alias).toBe(payload.alias);
        });
    });

    describe('and turn on/off a specific device...', () => {
        it('/PUT /devices/alias/:alias/relay -- should toggle device relay', async () => {
            // arrange
            const alias = 'test-plug';
            const payload = { relay_state: '1' };

            // act
            const response = await request(app.getHttpServer())
                .put(`/devices/alias/${alias}/relay`)
                .set('Authorization', `Bearer ${accessToken}`)
                .send(payload);

            // assert
            expect(response.status).toEqual(201);
            expect(response.body.alias).toBe(alias);
            expect(response.body.relay_state).toBe(Number(payload.relay_state));
        });

        it('/PUT /devices/alias/:alias/children/:childAlias/relay -- should toggle child device relay', async () => {
            // arrange
            const alias = 'spare-outdoor';
            const childAlias = 'test-child-plug-0';
            const payload = { relay_state: '1' };

            // act
            const response = await request(app.getHttpServer())
                .put(`/devices/alias/${alias}/relay/children/${childAlias}`)
                .set('Authorization', `Bearer ${accessToken}`)
                .send(payload);

            // assert
            expect(response.status).toEqual(201);
            expect(response.body.alias).toBe(childAlias);
            expect(response.body.state).toBe(Number(payload.relay_state));
        });
    });
});
