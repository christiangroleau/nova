import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { UsersModule } from '../src/users/users.module';
import { AuthModule } from '../src/auth/auth.module';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from '../src/auth/guards/auth.guard';
import { PrismaTestUtilitiesService } from './utils/prisma-test-utils';
import configuration from '../src/shared/configuration';

describe('When an admin queries users...', () => {
  let app: INestApplication;
  let prismaUtils: PrismaTestUtilitiesService;

  let accessToken = '';
  let accessTokenAdmin = '';

  const user = { username: 'users-john', password: 'password' };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AuthModule, UsersModule],
      providers: [
        {
          provide: APP_GUARD,
          useClass: AuthGuard,
        },
        PrismaTestUtilitiesService, // use test prisma service
      ],
    }).compile();

    app = moduleRef.createNestApplication();
    prismaUtils = app.get<PrismaTestUtilitiesService>(
      PrismaTestUtilitiesService,
    );
    await app.init();

    const responseAdmin = await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'admin', password: configuration().adminPassword });

    accessTokenAdmin = responseAdmin.body.access_token;
  });

  beforeEach(async () => {
    await prismaUtils.createUser(user.username, user.password);

    // capture the auto token prior to all of the tests
    const response = await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: user.username, password: user.password });
    accessToken = response.body.access_token;
  });

  afterEach(async () => {
    await prismaUtils.deleteUser(user.username);
  })

  afterAll(async () => {
    await prismaUtils.$disconnect();
    await app.close();
  });

  it(`/GET /users/me -- should allow an authenticated user to view the data`, async () => {
    // act & assert
    const response = await request(app.getHttpServer())
      .get('/users/me')
      .set('Authorization', `Bearer ${accessToken}`);

    expect(response.status).toEqual(200);
    expect(response.body.username).toBe(user.username);
  });

  it(`/GET /users/me -- should not allow an unauthenticated user to view the data`, async () => {
    // arrange
    const badAccessToken = '';

    // act & assert
    const response = await request(app.getHttpServer())
      .get('/users/me')
      .set('Authorization', `Bearer ${badAccessToken}`); // intentionally incorrect

    expect(response.status).toEqual(401);
  });

  it(`/GET /users/findByName for USER role -- should not allow a user to make use of an admin route`, async () => {
    // act
    const response = await request(app.getHttpServer())
      .get(`/users/${user.username}`)
      .set('Authorization', `Bearer ${accessToken}`);

    // assert
    expect(response.status).toEqual(403);
  });

  it(`/GET /users/all for USER role -- should not allow a user to make use of an admin route`, async () => {
    // act
    const response = await request(app.getHttpServer())
      .get('/users')
      .set('Authorization', `Bearer ${accessToken}`);

    // assert
    expect(response.status).toEqual(403);
  });

  it(`/GET /users/all for ADMIN role -- should allow an admin to make use of an admin route`, async () => {
    // act
    const response = await request(app.getHttpServer())
      .get('/users')
      .set('Authorization', `Bearer ${accessTokenAdmin}`);

    // assert
    expect(response.status).toEqual(200);
    expect(response.body[0].username).toBe('admin');
    expect(response.body[0].id).toBeTruthy();
  });
});
