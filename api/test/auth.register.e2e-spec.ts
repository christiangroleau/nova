import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { AuthModule } from '../src/auth/auth.module';
import { PrismaTestUtilitiesService } from './utils/prisma-test-utils';

describe('When a user registers an account...', () => {
  let app: INestApplication;
  let prismaUtils: PrismaTestUtilitiesService;
  const user = { username: 'register-john', password: 'password' };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AuthModule],
      providers: [PrismaTestUtilitiesService] // use test prisma service
    }).compile();

    app = moduleRef.createNestApplication();
    prismaUtils = app.get<PrismaTestUtilitiesService>(PrismaTestUtilitiesService);
    await app.init();
  });

  beforeEach(async () => {
    await prismaUtils.createUser(user.username, user.password);
  });

  afterEach(async () => {
    await prismaUtils.deleteUser(user.username);
  })

  afterAll(async () => {
    await prismaUtils.$disconnect();
    await app.close();
  });

  it(`/POST /auth/register -- should register a brand new user`, async () => {
    // arrange
    const success = await prismaUtils.deleteUser(user.username);

    // act & assert
    return request(app.getHttpServer())
      .post('/auth/register')
      .send({ username: user.username, password: user.password })
      .expect(201)
      .expect(function (res) {
        if (!res.body.hasOwnProperty('access_token'))
          throw new Error("Expected 'access_token' key!");
      });
  });

  it(`/POST /auth/register -- should not registers already existing user`, async () => {
    // act & assert
    return request(app.getHttpServer())
      .post('/auth/register')
      .send({ username: user.username, password: user.password })
      .expect(HttpStatus.CONFLICT);
  });
});
