### Getting started

```
npm install
npx prisma init
cp .env.sample .env
edit .env
npm run migrate:dev
```

See main README.md in the root of thie repo for more details