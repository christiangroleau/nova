# quick utility to give line of code count
wc -l `find . -not -path "./node_modules/*" -not -path "./dist/*" -not -path "./db/*" -type f \( -name "*.ts" \)`