# nova

![Release](https://img.shields.io/badge/alpha-2.0.0-blue?labelColor=grey&style=flat)
![Node](https://img.shields.io/badge/Node-18+-blue?labelColor=grey&style=flat)
![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)

An open source [TP-Link Kasa](https://www.kasasmart.com/us/products) REST API with a focus on local control, simplicity and, most of all, privacy.

Control and schedule most of your TP-Link Kasa devices locally and without the need of a cloud account, all with one small server.

**Note:** Nova is still in alpha. It is nearly feature complete but still undergoing significant changes.

## Getting started

Install all of the NPM dependencies

```
cd api
npm install
```

To configure Nova, start by creating your own custom .env file

```
cp .env.sample .env
```

Edit `.env` to include the server's listening address, database and jwt secret etc.

Something like the following:

```
# meta data
API_VERSION=1
DB_VERSION=1

# seed admin account 
ADMIN_USERNAME=admin
ADMIN_PASSWORD=password

# auth
JWT_EXPIRATION=600 # in seconds
JWT_SECRET=topsecret51

# server
PORT=3000

#db
DATABASE_URL=file:../db/dev.db
```

Prepare the database
```
npx prisma init
npm run migrate:dev
```

Run the server (currently only dev is supported)

```
npm run start:dev
```

A much more resilient setup with Docker/PM2 and Caddy will appear in an upcoming update.

## Adopting new devices privately

### Provisioning a device

The simplest way to provision a device (without using the official app) is to use [Python Kasa](https://python-kasa.readthedocs.io/en/stable/cli.html).

Power on the device and connect to its open network via wifi. The SSID will appear like TPLINK_... If the device does not broadcast a SSID it will need to be reset.

Once you are connect to the device, verify its IP address `(default: 192.168.0.1)`
```
kasa discover
```

Using that IP, see which networks are visible to the device:

```
kasa --host 192.168.0.1 wifi scan
```

Noting the SSID and keytype, the device can join the network with this command:

```
kasa --host 192.168.0.1 wifi join <SSID> --keytype <keytype> --password <password>
```

Connecting to the same network on your workstation, you may now discover the newly provisioned device:

```
kasa discover

== TP-LINK_Smart Dimmer_1CE5 - KS230(US) ==
	Host: 10.69.2.192
	Port: 9999
	Device state: True
	== Generic information ==
	Time:         2000-01-12 02:05:26 (tz: {'index': 6, 'err_code': 0}
	Hardware:     2.0
	Software:     1.0.7 Build 220729 Rel.091003
	MAC (rssi):   9C:A8:F4:60:3C:E6 (-60)
	Location:     {'latitude': 0.0, 'longitude': 0.0}

	== Device specific information ==
	LED state: True
	On since: 2024-05-30 21:26:17
	Brightness: 51

	== Modules ==
	+ <Module Schedule (schedule) for 10.69.2.192>
	+ <Module Usage (schedule) for 10.69.2.192>
	+ <Module Antitheft (anti_theft) for 10.69.2.192>
	+ <Module Time (time) for 10.69.2.192>
	+ <Module Cloud (cnCloud) for 10.69.2.192>
	- <Module Motion (smartlife.iot.PIR) for 10.69.2.192>
	- <Module AmbientLight (smartlife.iot.LAS) for 10.69.2.192>
```

## Supported devices

| Model                                                                                                    | Type               |
| -------------------------------------------------------------------------------------------------------- | ------------------ |
| HS100, HS103, HS105, HS107, HS110,<br/>HS200, HS210, HS220, HS300, KP303, KP400<br/>ES20M, EP40, ...etc. | Plug               |
| LB100, LB110, LB120, LB130, LB200, LB230, KL50, KL120, KL125<br/>...etc.                                 | Bulb               |
| KL430<br/>...etc.                                                                                        | Bulb (light strip) |

Many other TP-Link plug and bulb models may work as well. Cameras are not supported.

## Supported functionality

Nova exposes several endpoints

These include:
- Discover new devices on your network
- Power on/off light switches and plugs
- Schedule power on/off
- Customize device names (eg 'Front Door Light')
- Retrieve device configuration information

Best way to view them is through [Insomnia](https://insomnia.rest/) and import `api/test/insomnia/insomnia.json`

## Scheduling

Schedules are persisted through power outages and are unimpacted by loss of internet connection. Once power is restored all schedules are resumed as they were originally set. This is a big advantage over the official TP-Link offering.

# About

What started out as a way to deal with my backyard flooding to turn on/off a small pump has now grown into my full-home automation tool. The first version of this API has been running in my home for over a year.

## Dependencies
At its core, Nova is a [NestJs](https://nestjs.com/) service using [Prisma](https://www.prisma.io/) to track devices and schedules. Interacting with the TP-Link hardware is through the [PlasticRake](https://github.com/plasticrake/tplink-smarthome-api) low level library. Scheduling is supported by [@nestjs/schedule](https://docs.nestjs.com/techniques/task-scheduling).

Nova runs on Node 18+